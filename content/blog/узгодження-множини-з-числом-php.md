+++
title = "Узгодження множини з числом (PHP)"
date = 2012-01-31T19:43:06+00:00
[extra]
author = "yurb"
+++
Недавно треба було зробити на PHP зворотній лічильник днів до події. В таких випадках виникає потреба узгодити число днів, що залишились, з іменником «день». В сучасній українській мові є три способи узгодження з множиною:

* для всіх чисел, що закінчуються на 1, за винятком 11, маємо іменник в однині («день»)
* для тих, що закінчуються на 2, 3, 4, за винятком 12, 13, 14, маємо множину в назиному відмінку («дні»)
* для решти — множина в родовому відмінку («днів»)

Окрім того, в мові існувала ще [*двоїна*](http://uk.wikipedia.org/wiki/Двоїна), але тут ми її обійдемо. Словом, ось код:

<!-- more -->

```
switch ($daysleft % 10) { // Це не я придумав
case 1:
	if ($daysleft != 11) {
		$output .= ' день';
		break;
	}
case 2:
case 3:
case 4:
	if (($daysleft < 10) || ($daysleft > 20)) {
		$output .= ' дні';
		break;
	}
default:
	$output .= ' днів';
}
```
