+++
title = "Дві слові про паролі"
date = 2014-10-18T11:25:03+00:00
[extra]
author = "yurb"
+++
<figure>
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/KeePassX-uk.png/512px-KeePassX-uk.png" alt="KeePassX"></img>
  <figcaption>Менеджер паролів KeePassX</figcaption>
</figure>

Не можу не поділитися однією простою й дуже розумною [думкою одного дослідника компуторної безпеки](https://diogomonica.com/posts/password-security-why-the-horse-battery-staple-is-not-correct/):

Запам'ятовувати усі паролі — **дурна ідея.** Вони обов'язково будуть слабкими і легкими до зламування. Хіба не легше використовувати **менеджер паролів**, і мати при тому супер-міцні паролі, які не треба тримати в голові?

<!-- more -->

Менеджер паролів — програма, яка дає можливість безпечно зберігати Ваші паролі на компуторі в зашифрованій формі. Щоб отримати доступ до паролів, треба вписати свій головний пароль — і тільки його і треба пам'ятати. Самі ж паролі до веб-сайтів пам'ятати не треба, і вони можуть бути якими-завгодно довгими і складними. І що найважливіше — для кожного веб-сайту можна мати окремий, неповторний пароль. 

Деякі менеджери паролів підтримують функцію «самовдруковування» (autotype), що дає можливість вибрати потрібний пароль, натиснути одну кнопку, і програма сама впише його у вікно веб-переглядачки. Таким чином немає потреби вручну копіювати пароль у буфер і кудись вставляти.

Чи Ви знали, що в одній базі викрадених паролів з Adobe, опублікованих в Інтернеті, у [понад мільйона людей був однаковісінький пароль](http://stricture-group.com/files/adobe-top100.txt)? І знаєте яким він був? Правильно, `123456`. А ще у понад 300 тис. — слово `password`.

Звісно, так роблять тільки якісь диваки, і ми з Вами таких паролів не використовуємо. Але зате ми часом полюбляємо користуватись одим і тим же паролем на різних сайтах, або ж зберігати паролі у текстовому файлі на компуторі чи записувати їх у блокноті. Дехто полюбляє використовувати як пароль свій день народження. Усім цим ми пиляємо гілку, на якій сидимо.

Тому забудьте про усі ці способи «обійти» проблему паролів, і заінсталюйте якийсь надійний менеджер паролів, наприклад [KeePass](http://keepass.info/) або [KeePassX](https://www.keepassx.org/). Придумайте один надійний довгий пароль для вашої бази даних, а решту паролів генеруйте в програмі, і робіть довгими — скажімо 30+ знаків. І не забудьте час від часу копіювати свою базу даних на флешку, щоб у разі чого могти її відновити.
