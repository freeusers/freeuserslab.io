+++
title = "bc як конвертор основи"
date = 2012-02-28T15:43:29+00:00
[extra]
author = "dor"
+++
Утиліту [bc](http://www.google.com.ua/search?q=man+bc) (an arbitrary precision calculator language) можна досить зручно використовувати для конвертування чисел між основами, в тому числі у скриптах:

```
$ echo "ibase=16; obase=2; 5F" | bc
1011111

$ echo "ibase=16; obase=8; 32" | bc
62

$ echo "ibase=10; obase=4; 12" | bc
30

```

Може, комусь і згодиться.

<!-- more -->
