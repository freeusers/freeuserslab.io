+++
title = "Письменництво з Git"
date = 2013-03-27T16:53:00+00:00
[extra]
author = "yurb"
+++
Протягом певного часу я застосовую т.зв. керування версіями до текстів. Це дає мені можливість:

1. бачити зміни, які я зробив відносно останньої «зафіксованої» версії файлу;
2. порівнювати між собою будь-які з раніших версій файлу;
3. паралельно працювати над кількома варіянтами того ж файлу, які відрізняються у певних місцях, але мають значну частину спільного тексту; тобто спробувати, наприклад, переробити певний розділ і подивитись «що із того вийде», залишивши стару версію цього розділу доступною до повернення в документ навіть після будь-якого редагування инших розділів;
4. синхронізувати версію одного файлу із версією другого (наприклад, текст із ілюстрацією);
5. відіслати комусь документ для редагування й переглянути усі корекції при отриманні; обирати, яку корекцію прийняти, а яку ні.

 Це далеко не вичерпний перелік. Як це працює? У вас є тека з файлами, а всередині цієї теки є під-тека `.git`, де зберігаються попередні версії файлів та інформація про послідовність та зв'язок між змінами. Єдина вимога — навчити вашу систему керування версіями порівнювати різні версії файлів того формату, в якому ви пишете ваш текст.

<!-- more -->

Що таке керування версіями
----------

Про це можна розповідати безконечно — настільки багатогранною є ця сфера. Але основна ідея ось у чому.

 Образно кажучи, ваш документ перетворюється на рослину. Ви вже не пишете текст, а вирощуєте його (операція `git commit`). При тому стебло вашої рослини може розгалужуватись (`git branch`), ви можете розглядати її з різних боків і на різних «щаблях розвитку» (`git checkout`). Ви можете навіть робити дещо незвичне: об'єднати галузки в одну (`git merge`) і переносити «ягідки» з однієї гілки на другу (`git cherry-pick`).

 Увесь ваш текст стає нашаруванням поліпшень. Ви постійно додаєте щось нове. І ви можете бачити, що саме й коли ви додали, скасовувати це в будь-який момент. Ви можете щось написати увечері, а зранку подивитись, що саме із цих редагувань залишити, а що переробити: ви можете бачити будь-які зміни, які ви зробили відносно до будь-якої версії.

 Керування версіями виникло спершу як засіб для програмарів, бо вони мусять працювати колективно, і дуже скрупульозно контролювати, що саме, хто й коли зробив, щоб не витерти чужу роботу. Але останнім часом керування версіями почали застосовувати до найрізноманітніших типів інформації, навіть 3-вимірної графіки. Письменництво — це, мабуть, друга після програмування найдоречніша сфера застосування керування версіями.

 До Git є гарний [підручник, перекладений українською мовою](http://xenon.stanford.edu/~blynn/gitmagic/intl/uk/), і, здається я ліпше про Git не розповім, ніж це зроблено там.

 Загалом, вам доведеться витратити трохи часу на освоєння Git, але після цього ваше ставлення до роботи з інформацією повністю зміниться. Воно того вартує.

Тип файлу для вашого тексту
----------

Усі файли діляться на дві основні категорії: умовно текстові та двійкові. Текстові — це такі, у яких кожен байт (чи група байтів) означають певну літеру чи символ. Двійкові — такі, де дані можуть бути будь-чим, зокрема й текстом, але які неможливо прочитати безпосередньо як текст.

 Тому, наприклад, файл формату DOC *не є* текстовим у цьому значенні: бо крім, власне, символів він містить числовим способом закодовану інформацію про вигляд тексту і т. д. А ось, наприклад, графічний файл SVG є текстовим, бо всю інформацію в ньому записано в текстовій формі (XML).

 Будь-який текстовий файл можна відкрити програмою на кшталт Віндовсового Блокнота (з тією різницею, що Блокнот не розрізняє невіндовсового символа нового рядка).

 Системи керування версіями ідеально працюють із не-двійковими файлами, бо тоді вони «бачать файл наскрізь», і можуть добре орієнтуватись в тому, які зміни зроблено у файлі, або навіть самостійно об'єднувати різні версії файлу, тобто забезпечити повний набір функцій.

 Але існує також спосіб використання «напів-двійкових» форматів, як то ODT чи DOCX (обидва формати є двійковим архівом ZIP із не-двійковими файлами усередині). Тут я безпосередньо опишу лише спосіб роботи із не-двійковими файлами, оскільки сам працюю із ними, але також поділюсь лінками на підказки щодо роботи із документами ODT та DOCX.

### Файли ODT або DOCX ###

Якщо ви вбачаєте потребу використовувати традиційний «офіс» для написання ваших текстів, у вас є спосіб скористатись системою керуванням версіями. Для цього вам треба зберігати ваші файли у форматі ODT або DOCX. ODT, тобто OpenDocument Text — відкритий стандарт для текстових документів «офісного» типу. «Рідний» формат пакетів LibreOffice, OpenOffice, Koffice та инших; підтримується Microsoft Office починаючи від версії 2007 SP2. DOCX — теж відкритий стандарт, «рідний» формат Microsoft’івських офісів від Office 2007.

 Як я вже казав, щоб із керування версіями для тексту була якась користь, `git` повинен уміти якось порівняти два файли між собою, тобто з'ясувати, які саме рядки у ньому змінено, а які ні.

 Оскільки я сам не використовую цих форматів, можу тільки переповісти досвід инших людей щодо цього. У Windows існує знаряддя для роботи з `Git` під назвою [TortoiseGit](https://code.google.com/p/tortoisegit/), яке уміє порівнювати як файли ODT, так і файли DOCX.

 Для користувачів инших систем існують окремі програми, якими `git` може скористатись для порівняння цих документів. Для ODT існує програма `odt2txt`, для DOCX — `docx2txt`. Ось [інструкція щодо ODT із Mercurial, SVN та Git](http://www-verimag.imag.fr/~moy/opendocument/), а ось дещо про [DOCX із Git](http://techblog.vsza.hu/posts/Tracking_history_of_docx_files_with_Git.html) у \*nix’ах.

### Не-двійковий текст ###

Існує багато способів писати тексти у, власне, текстових файлах, але при тому мати можливість керувати форматуванням тексту. Кожен має свої переваги і недоліки. Я використовую [Emacs Org-Mode](http://uk.wikipedia.org/wiki/Org-mode), перевагою якого є можливість конвертування як у формат [ODT](http://uk.wikipedia.org/wiki/OpenDocument) (для традиційних «офісів»), так і в [LaTeX](http://uk.wikipedia.org/wiki/LaTeX), який генерує PDF’и, що мають значно професійніший вигляд, ніж те, що видають «офіси», а також HTML (власне, цей допис я теж написав у Org-mode). Недоліком Org-mode є залежність від текстового редактора Emacs.

 Але ви можете писати безпосередньо і файли LaTeX, що я теж робив, поки не натрапив на Org-mode. Дуже багато людей пишуть файли LaTeX безосередньо, і, зрештою, він для того й придуманий. До LaTeX є детальний та легкий до освоєння [підручник українською мовою](http://mirrors.ctan.org/info/lshort/ukrainian/lshort-ukr.pdf).

 Основна перевага більшости способів написання текстових документів таким чином — це те, що ви зазначаєте, насамперед, *структуру* вашого документа, а вигляд формується автоматично на основі цієї структури.

 Тобто замість робити великий шрифт там, де буде заголовок, ви позначаєте, що це слово є заголовком, а програма не просто форматує текст відповідним чином, але *знає, що тут буде новий розділ*, і, наприклад, автоматично генерує для вас зміст документа<sup><a class="footref" name="fnr-.1" href="#fn-.1">1</a></sup>, різноманітні покажчики і т.д.

 Отож, кожне завдання можна розв'язувати багатьма способами, деякі з них можуть бути значно цікавіші й охайніші за традиційні. Одним з таких способів є використання керування версіями для будь-якої відповідальної роботи, зокрема текстової, що неодмінно заохотить вас дбайливо піклуватись про ваш текст, не боячись при тому із ним експериментувати.

Примітки:
----------

<sup><a class="footnum" name="fn-.1" href="#fnr-.1">1</a></sup> Звичайно, й «офіси» можуть «розуміти» структуру документа, але більшість людей чомусь цим не користуються, і форматують документ вручну.
