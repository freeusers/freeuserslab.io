+++
title = "Веб-сайти на localhost — автоматичне налаштування virtual hosts і dns (fedora)"
date = 2015-05-14T12:50:39+00:00
[extra]
author = "yurb"
+++
**TLDR:** Правда, було б зручно, якби просто створивши теку в `~/public_html/`, ми могли відразу і автоматично бачити її за адресою `http://назва-теки.self/` як повноцінний віртуальний сервер апача? Ось один зі способів це зробити (я користуюся Fedora, але, можливо, на инших дистрибутивах процедура буде подібною). **ОНОВЛЕНО:** *Змінено суфікс на `.self`, оскільки `.dev` ­­­— це від якогось часу [справжній домен верхнього рівня](https://en.wikipedia.org/wiki/List_of_Internet_top-level_domains#English)*. Я припускаю, що налаштуваннями мережі у Вас керує NetworkManager, а веб-сервер — апач. Якщо у Вас якась инакша ситуація, процедуру треба (звичайно) скорегувати відповідно до потреб.

<!-- more -->

1. Налаштовуємо автоматичну переадресацію всіх адрес, які завершуються на `.self`, на Ваш локалгост. Для цього створюємо файл `/etc/NetworkManager/dnsmasq.d/vhosts` з таким вмістом:

   ```
   address=/self/127.0.0.1

   ```

   А також створюємо файл `/etc/NetworkManager/conf.d/vhosts.conf` з таким вмістом:

   ```
   [main]
   dns=dnsmasq

   ```

   Перезавантажуємо NetworkManager: `systemctl restart NetworkManager` й перевіряємо, чи працює переадресація DNS:

   ```
   [user@localhost]$ resolveip whatever.self
   IP address of whatever.self is 127.0.0.1

   ```

2. Налаштовуємо апач для автоматичних virtual hosts. Для цього створюємо файл `/etc/httpd/conf.d/vhosts.conf` з таким змістом:

   ```
   UseCanonicalName off
   VirtualDocumentRoot /home/user/public_html/%1

   <Directory /home/user/public_html/>
           Require local
           # Потрібно для drupal, можливо ненайбезпечніше
   	AllowOverride All
   </Directory>

   ```

Щоб це працювало, треба щоб в апачі був увімкнутий модуль `[mod_vhost_alias](https://httpd.apache.org/docs/2.2/mod/mod_vhost_alias.html)` (у мене був увімкнутий «з коробки»). Також треба впевнитися, що `[selinux](https://uk.wikipedia.org/wiki/Selinux)` не «битиме апач по руках» за спробу доступу до теки public\_html. Для цього повинно бути достатньо запустити команду `chcon --recursive --type httpd_user_content_t /home/user/public_html/` і впевнитися, що користувач `apache` має права читання потрібних файлів і запису до тих тек, які повинні змінюватися сервером (наприклад, `sites/all/files` у drupal). 

Перезавантажуємо апач: `systemctl restart httpd`. Створюємо теку `~/public_html/**whatever**/` й кидаємо туди якийсь html-файл. Перевіряємо, чи працює, відкривши у веб-переглядачці `http://**whatever**.self/наш-html-файл.html`. Якщо щось не працює, дивимось у `/var/log/httpd/error.log` і у `/var/log/audit/audit.log` за підказками.

Сподіваюся, нічого не забув:) Приємної праці.
