+++
title = "ffmpeg: стрибки у входовому файлі"
date = 2012-02-23T22:44:57+00:00
[extra]
author = "yurb"
+++
Якщо ви, так само як і я чи [цей користувач](http://ffmpeg.org/pipermail/ffmpeg-user/2011-April/000221.html), скориставшись параметром **-ss** у **ffmpeg**, довго чекаєте, поки програма стрибне у потрібне місце, або при тому отримуєте від неї помилки (чи навіть, як у мене, вона в певний момент перелазить через оперативку й помирає), *поставте* **-ss**  *перед* **-i**. Помилка-індикатор:

```
Buffering several frames is not supported. Please
consume all available frames before adding a new one.
```

<!-- more -->
