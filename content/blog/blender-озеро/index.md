+++
title = "Blender: озеро"
date = 2012-02-23T23:33:47+00:00
[extra]
author = "yurb"
+++
Хтось (Jonathan Esquivel) зробив дуже гарний, при тому не задовгий (12хв) швидкоплин (timelapse) створення озера у Blender:

<!-- more -->

[![youtube-відео створення озера](https://img.youtube.com/vi/C69wUfySBGI/0.jpg)](https://www.youtube.com/watch?v=C69wUfySBGI)

 Джерело: [blendernation.com](http://www.blendernation.com/2012/02/23/timelapse-lake/#utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+Blendernation+%28BlenderNation%29)
