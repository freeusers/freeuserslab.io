+++
title = "Пошук українською"
date = 2012-08-08T07:02:45+00:00
[extra]
author = "dor"
+++
Задача — хочу шукати у мережі українською мовою. Пошуковики, як на мене, видають забагато результатів російською, це не відповідає моїй внутрішній політиці пріоритетів щодо рейтингів сайтів blah-blah-blah. Хочу --- і все.

<!-- more -->

Для мене — користуюся Firefox’ом та шукаю в Google --- вирішення виявилося досить простим: почитав про [параметри пошуку Google](https://moz.com/blog/the-ultimate-guide-to-the-google-search-parameters), почитав по [створення модуля пошуку для Firefox](https://developer.mozilla.org/en-US/Add-ons/Creating_OpenSearch_plugins_for_Firefox) і зробив.

~~Сподіваюся, для тих, хто користується іншими переглядачами/пошуковиками, також щось таке знайдеться. Трохи згодом спробую розширити свої знання з цього приводу і поділитися.~~

**Виявилося, це чудово працює і для Google Chrome** — треба лише у віконечку, що вигулькне, змінити «ключове слово» з google.com на щось інше, та у налаштуваннях пошуку Chrome знайти MyGoogle серед «Інших пошукових систем» та встановити його «за умовчанням». І все чудово працює .)

Також замість пошуковика Google можна вписати (у XML файл, див. нижче) будь-який інший, треба лише знати параметри запиту (їх назви) для здійснення пошуку.

**Не знаю, чи працюватиме це як треба *скрізь*** :О) Бо використовується `ie=uft-8&oe=utf-8`. Цікаво, що буде, якщо їх не вказувати зовсім; спробую пізніше.

### Власне, «модуль пошуку», XML файл ###

Отже, створюємо отакий файл (називаємо його `mygoogle.xml`):

```xml
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/"
                       xmlns:moz="http://www.mozilla.org/2006/browser/search/">
  <ShortName>MyGoogle</ShortName>
  <Description>MyGoogle</Description>
  <Image width="16" height="16">data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAaRJREFUeNpiVIg5JRURw0A0YAHio943kYV%2B%2Ff33%2BdvvX7%2F%2FMjEx8nKycrGzwKXOiPKzICvdeezLhCV3jp15%2Bfv%2FX0YGhv8MDDxMX2qKTIw0RK10eYD6QYqATvoPBkt3f5K0W9Ew4fjTFz%2F%2Bw8Dm3W8UPeZxqFa%2BevsFyD0twgfVsOfkRxHrtfV9u5BVQ8Crd98%2FffkGYQM1QJ20%2FfSPv79eNxQGYfpSVJADmcvEAHbr7oOX2dj%2FERNKIA2%2F%2F%2Fz%2FxfCDhYVoDUDw5P6vf9%2B5iY0HVmZGQWm%2BN3fff%2Fn2k4eLHS739x%2FDiRs%2Ff%2F%2F5x8HO%2FOHzN3djfqgNjIwMgc6qzLx%2Fpy47j2zY%2Feff06tXhOUucgxeun33AUZGpHh4%2Bvo7t8EyIJqz%2FhpasD59%2B5dNrqdnznZIsEL9ICXCsWuBCwvTv%2FymS5PWPP32ExEALz%2F%2BB5r848cPCJcRaMP9xaYQzofPPzfuvrnj0Jst%2B5%2F8%2Bc4sLPeDkYlRgJc93VPE18NIXkYUmJYQSQMZ%2FP3379uPH7%2F%2F%2FEETBzqJ0WqLGvFpe2LCC4AAAwAyjg7ENzDDWAAAAABJRU5ErkJggg%3D%3D</Image>
  <InputEncoding>UTF-8</InputEncoding>
  <Url type="application/x-suggestions+json" method="GET" template="http://suggestqueries.google.com/complete/search?output=firefox&amp;client=firefox&amp;hl={moz:locale}&amp;q={searchTerms}"/>
  <Url type="text/html" method="GET" template="http://www.google.com/search">
    <Param name="q" value="{searchTerms}"/>
    <Param name="ie" value="utf-8"/>
    <Param name="oe" value="utf-8"/>
    <Param name="aq" value="t"/>
    <Param name="hl" value="uk"/>
    <Param name="lr" value="lang_uk|lang_en"/>
    <moz:SearchForm>http://www.google.com/search</moz:SearchForm>
  </Url>
</OpenSearchDescription>
```

 Можна додати/вилучити мови за смаком, таким чином:

```xml
    <!-- так додаємо французьку: -->
    <Param name="lr" value="lang_uk|lang_en|lang_fr"/>
    <!-- так виключаємо російську: -->
    <Param name="lr" value="lang_uk|lang_en|-lang_ru"/>
    <!-- будь-яка, не російська: -->
    <Param name="lr" value="-lang_ru"/>
```

 З приводу мови пошуку Google пише: «If there are no results in the specified language, the search appliance displays results in all languages».

### Сторінка, з якої можна встановити модуль, HTML файл ###

 Створюємо якийсь такий html:

```html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
  <head>
    <title>MyGoogle search engine loader</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript">
      /*
       * See some docs here:
       * https://developer.mozilla.org/en-US/Add-ons/Creating_OpenSearch_plugins_for_Firefox
       * https://developer.mozilla.org/en-US/docs/Web/API/Window/sidebar/Adding_search_engines_from_Web_pages
       */
      function addEngine(engineURL) {
        if (window.external && ("AddSearchProvider" in window.external)) {
          // Firefox 2 and IE 7, OpenSearch
          window.external.AddSearchProvider(engineURL);
 
        } else if (window.sidebar && ("addSearchEngine" in window.sidebar)) {
          // Firefox <= 1.5, Sherlock
          window.sidebar.addSearchEngine(engineURL,
                                   "http://example.com/search-icon.png",
                                   "My Google", "");
 
        } else {
          // No search engine support (IE 6, Opera, etc).
          alert("No search engine support detected");
 
        }
      }
      function addEngine(engineURL) {
        if (window.external && ("AddSearchProvider" in window.external)) {
          window.external.AddSearchProvider(engineURL);
          return false;
        } else {
          alert(error_opensearch_unsupported);
          return true;
        }
      }
     </script>
  </head>
  <body>
    <div id="wrapper">
       <input type="button" value="Додати «пошуковик» MyGoogle" onclick="return addEngine('https://freeuser.org.ua/wp-content/themes/twentyfifteen/js/mygoogle.xml')" />
    </div>
  <body>
</html>
```

### Результат ###

 Ідемо на створену сторінку, клацаємо на кніпочці.

### Примітки ###

1. Почук через «MyGoogle» працюватиме у вікні пошуку, але якщо щось ввести у рядку адреси і тицьнути \<Enter\> — шукатиме не там. Щоб і там шукало «там» (так, я тавтолог,), треба полізти у налаштування і дещо змінити, див. знімок екрану (можна клацнути, it's fancyboxed):[![Як вказати MyGoogle як «головний пошуковик»](https://brownian.org.ua/wp-content/uploads/2012/07/Знімок-екрана-з-2012-07-19-111639-300x145.png)](https://brownian.org.ua/wp-content/uploads/2012/07/Знімок-екрана-з-2012-07-19-111639.png)
2. Можна поставити `lr=-lang_ru`, Google видаватиме результати **не** російською мовою, але у прямокутничку на блакитному тлі писатиме «Шукати сторінки такою мовою: російська». Це в них бага, на результат не впливає, але все одно неприємно :О)
3. Крім того, якщо мене не влаштовує домен **`.ru`** — чи, [з якихось причин](http://w3fools.com/), сайт www.w3schools.com --- я можу у `searchTerms` написати таке (замість того, що вище):

   ```xml
    <Param name="q" value="{searchTerms}+-ru+-w3schools"/>
   ```

   Інших параметрів купа, якщо цікаво — я дав посилання вище, почитайте.

4. Якщо захочете перевстановити чи просто видалити — шукайте у поточному профілі Firefox’а каталог `searchplugins`, у ньому `mygoogle.xml`. Перед перевстановленням треба закрити Firefox, видалити той файл, знову запустити Firefox і знову натиснути кніпочку.
