+++
title = "Календар нашвидкуруч"
date = 2012-02-28T13:50:55+00:00
[extra]
author = "dor"
+++
1. Запускаємо [inkscape](http://inkscape.org/). Розгортаємо на весь екран, натискаємо п’ятірку («вписати документ у вікно»).
2. *Додатки –\> Відтворити –\> Календар*
3. На закладці «Локалізація» вписуємо назви місяців, днів тижня; на закладці «Кольори» можемо експериментувати з кольорами. Вибираєте системне кодування:

<!-- more -->

[![](Calendar-settings.png)](Calendar-settings.png)

О, до речі, у «Налаштуваннях» вибираємо понеділок як перший день тижня.

4. Натискаєте «Застосувати». Як бачите, вікно налаштувань не ховається; при цьому на календарі можна клацнути (виділити його), щоб редагувати, посунути, видалити тощо:[![](Calendar-done.png)](Calendar-done.png)
5. Тобто, зараз календар можна видалити, змінити налаштування і знов натиснути «Застосувати».

### Треба додати кілька днів народження? ###

1. Виділяємо календар (клацаємо на ньому).
2. Розгруповуємо тричі (тричі натискаємо Ctrl+U) — маємо таку картину:[![](Calendar-ungrouped.png)](Calendar-ungrouped.png)
3. Знімаємо виділення (клацаємо десь збоку), виділяємо акуратно (з shift’ом) потрібні дні (масштабуємо за допомогою Ctrl+wheel, соваємо «полотно» за допомогою Ctrl+RightButton) — я виділив три дні навмання, 11.04, 22.05, 2.08:[![](Calendar-someselected.png)](Calendar-someselected.png)
4. Натискаємо Ctrl+Shift+F (ось чому я не люблю Ctrl+Shift для перемикання розкладки!), вибираємо потрібний колір, можна і колір і товщину штриха (контур навколо літер):[![](Calendar-reddays.png)](Calendar-reddays.png)

### Що іще? ###

Іще — перед початком генерування календаря можна було на тло кинути кілька зайчиків чи лиса .)

Після завершення форматування власне календаря варто його назад згрупувати.

І взагалі — краще сам календар і зайчиків робити на різних рівнях.

Це все — домашнє завдання.

До речі ж, після завершення роботи зберігаємо у потрібному форматі — pdf, ps, експортуємо у png тощо. Але варто зберегти і «оригінальний» Inkscape SVG.

Все це працює й під Windows.
