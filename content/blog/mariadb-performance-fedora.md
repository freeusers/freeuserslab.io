+++
title = "Mariadb: швидкодія «з коробки» (Fedora)"
date = 2017-08-12T21:39:52+00:00
[extra]
author = "yurb"
+++
Mariadb «з коробки» має досить консервативні налаштування, не орієнтовані на швидкодію. Наприклад, [вимкнутий кеш запитів](https://mariadb.com/kb/en/mariadb/server-system-variables/#query_cache_type). Також для таблиць InnoDB сервер буде після кожного INSERT/UPDATE-запиту [вимагати від ядра записати зміни на диск](https://mariadb.com/kb/en/mariadb/xtradbinnodb-server-system-variables/#innodb_flush_log_at_trx_commit) (вилити їх з кешу файлової системи), що може бути дуууже повільно для більшої кількости запитів. Тому я змінив деякі типові налаштування, щоб пришвидшити роботу (тут не йдеться про якийсь production-сервер, а суто оптимальне середовище для розробки). Отже вміст файлу `/etc/my.cnf.d/performance.cnf`:

<!-- more -->

```
[server]
# Увімкнути кеш запитів і збільшити до 50M
query_cache_size = 52428800

# Зливати дисковий кеш кожні 30 с. замість після кожного
# INSERT/UPDATE
innodb_flush_log_at_trx_commit = 2
innodb_flush_log_at_timeout = 30

```
