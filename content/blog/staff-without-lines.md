+++
title = "LilyPond: Нотоносець без ліній"
date = 2012-02-22T23:07:45+00:00
[extra]
author = "yurb"
+++
У [LilyPond](http://lilypond.org/) є можливість друкувати ноти на невидимому нотному стані. Таке може стати в пригоді, наприклад, в навчальному процесі чи алеаторичній музиці. Ось як це робиться:

<!-- more -->

```lilypond
\override Staff.StaffSymbol #'transparent = ##t
```

Цю команду слід додати прямо перед нотами. Якщо ж Вам потрібно щоб невидимими стали й тактові риски, можете скористатись додатково ще такою командою:

```lilypond
\override Staff.BarLine #'transparent = ##t
```

Щоб відключити показ номерів тактів, слід додати блок

```lilypond
\layout {
  \context {
	\Score
	\remove "Bar_number_engraver"
  }
}

```

поза будь-якими блоками \\score, або у якомусь із них, якщо потрібно це застосувати лише до одного розділу твору. Більше про приховання окремих об’єктів читайте [тут](http://lilypond.org/doc/v2.14/Documentation/notation/visibility-of-objects) (англ.).
