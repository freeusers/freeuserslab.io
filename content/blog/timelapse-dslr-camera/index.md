+++
title = "Запис швидкоплину (timelapse) з допомогою фотокамери"
date = 2018-05-21T20:59:55+00:00
[extra]
author = "yurb"
+++
![Одноплатковий комп'ютер керує камерою](camera-timelapse.jpg)

* На штатив до камери прикріплено одноплатковий комп'ютер, який живиться від powerbank'у. Я використав [CHIP](https://en.wikipedia.org/wiki/CHIP_(computer)), але, на жаль, їх уже не виробляють. [Raspberry Pi](https://uk.wikipedia.org/wiki/Raspberry_Pi), мабуть, працював би не гірше.
* Він керує камерою через USB.
* На одноплатковому комп'ютері запущено debian, для керування камерою використовується [gphoto2](https://packages.debian.org/stretch/gphoto2).
* Ноутбук, коли є потреба, працює як точка доступу wifi, до якої автоматично під'єднується одноплатковий комп'ютер. З ноутбука я через ssh запускаю/зупиняю процес фотографування.

<!-- more -->

Програмна частина надзвичайно проста: `gphoto2` дає можливість керувати камерою через USB та робити періодичні знімки. Після під'єднання камери (за умови, що вона підтримується `gphoto2`) потрібно було запустити всього дві команди. Перша команда [встановлює місце](https://github.com/gphoto/libgphoto2/issues/197#issuecomment-323932325), куди зберігатимуться знімки — у моєму випадку на карту пам'яти фотоапарата:

```
gphoto2 --set-config capturetarget=1

```

Друга команда запускає власне процес ритмічного фотографування:

```
gphoto2 --capture-image -I 5

```

Параметр `-I` визначає інтервал між знімками (у секундах).

Альтернативи
----------

Моє рішення було найшвидшим і найпростішим у моїх умовах, натомість ваші умови можуть бути иншими. Ось кілька альтернатив:

* [TriggerTrap Mobile](https://en.wikipedia.org/wiki/Triggertrap#Triggertrap_Mobile), але воно вимагає придбання адаптера, який відрізняється залежно від моделі камери, і коштує дорожче, ніж Raspberry Pi.
* Програма для Android: [DSLR Controller](https://dslrcontroller.com/), вона є платною і схоже невільною (безкоштовні рекламно-підтримувані програми не розглядаю) + адаптер USB OTG.
* Саморобний пульт на основі Arduino ([аналоговий](http://www.paulodowd.com/2013/02/arduino-remote-trigger-for-dslr-stop.html) і [цифровий/PTP](https://www.circuitsathome.com/camera-control/arduino-based-controller-for-canon-eos-cameras/) варіянти). І це, мабуть, найкраще рішення, але воно вимагало б більше часу на підготування.
* Можливо, також існують «готові» пульти, які мають функцію ритмічного фотографування.
