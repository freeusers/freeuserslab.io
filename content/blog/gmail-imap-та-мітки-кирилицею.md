+++
title = "Gmail, IMAP та «мітки» кирилицею"
date = 2013-08-01T14:42:16+00:00
[extra]
author = "dor"
+++
Gmail дає змогу чіпляти до листа різні мітки, які з точки зору протоколів витягування пошти (POP3 та IMAP4) є скриньками. В принципі, інтерфейс Gmail також їх показує як скриньки з листами. Іноді у різних рецептах (згодувати `spamassassin`’у теку «Спам» із гуглопошти, наприклад) окремим пунктом програми передбачено витягування вмісту такої скриньки (за допомогою `fetchmail`, скажімо).

<!-- more -->

Рішення просте:

```
poll imap.gmail.com protocol IMAP
   user "nouser@gmail.com" is localuser here
   password 'superpassword',
   folder "[Gmail]/Spam",
   # folder 'from Smith',
   # fetchlimit 1, keep,
   ssl

```

 Але проблема виникає тоді, коли мітки українською (не латинкою, взагалі кажучи). І ніде, чомусь, я рішення не знайшов, саме тому це й пишу. Справа в тому, що кодування назв скриньок має бути у «модифікованому UTF-7»; це зазначено у [RFC 3501](https://tools.ietf.org/html/rfc3501) (5.1.3. Mailbox International Naming Convention). При цьому «службові» скриньки зазначаються як «[Gmail]/Скринька»:

```
poll imap.gmail.com protocol IMAP
   user "nouser@gmail.com" is localuser here
   password 'superpassword',
   folder "[Gmail]/&BCEEPwQwBDw-",         # «Спам» — обов’язково "[Gmail]/"
   # folder '&BDIEVgQ0- &BCEEMARIBDoEMA-', # «від Сашка» —  *без* "[Gmail]/"
   # fetchlimit 1, keep,
   ssl

```
