+++
title = "git та українські назви файлів"
date = 2012-07-24T23:33:00+00:00
[extra]
author = "yurb"
+++
Якщо `git status` показує українські назви файлів як `«"\320\277\321..."»`, спробуйте запустити комадну `git config --global core.quotepath off`. (Запозичено з [stackoverflow](http://stackoverflow.com/a/5855213/1142039)).

<!-- more -->
