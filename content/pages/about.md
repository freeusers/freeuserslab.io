+++
path = "/about/"
title = "Що таке freeUser?"
+++

**freeUser.org.ua** — українськомовний ресурс для занотовування
рецептів та корисного досвіду з використання
[вільних](http://uk.wikipedia.org/wiki/Вільні%20програмні%20засоби "Що таке вільні прогами") програм для практичних цілей. **freeUser** створено не з
комерційних міркувань, а через відчуття потреби в такому ресурсі — нам
його просто бракувало. Разом з тим, **freeUser** має визначений обсяг:
зберігати відносно короткі статті про *виконання того чи іншого
завдання* вільними програмами, і ми закликаємо авторів звернутись до
[Вікіпідручника](http://uk.wikibooks.org/ "Вікіпідручник — ресурс для колективного написання документів підручникового типу"), якщо ви бажаєте долучитись до створення документів
підручникового типу.

# Правила й принципи

1.  Публікуючи матеріали тут, Ви застосовуєте до них ліцензію [Creative
    Commons Attribution Share-Alike Unported
    3.0](http://creativecommons.org/licenses/by-sa/3.0/ "Умови ліцензії (англ.)"). Це, насамперед, означає, що
    -   опулікований вміст можна буде безкоштовно використовувати і
        розповсюджувати за умови, що
    -   буде збережено Ваше авторство та
    -   розповсюджувач буде публікувати твір і власні похідні від нього
        на тій самій ліцензії.
2.  Дозволяється публікувати статті лише з наступних тем:
    -   Виконання того чи іншого завдання, використовуючи ВПЗ. При тому
        завдання не повинно бути таким, що порушує закони моралі,
        авторські права, чинне законодавство. Також воно не повинно бути
        безглуздим.
    -   Проведення маніпуляцій із самими вільними програмами:
        налаштування, переклад, внесення модифікацій Вам необхідних,
        подання результату розробникам.
    -   Співпраця із мережевими ресурсами, побудованими навколо
        [вільного
        вмісту](http://uk.wikipedia.org/wiki/%D0%92%D1%96%D0%BB%D1%8C%D0%BD%D0%B8%D0%B9_%D0%B2%D0%BC%D1%96%D1%81%D1%82). Статті на цю тему повинні бути орієнтовані на
        практичні завдання, а не на опис того чи іншого ресурсу.
3.  Дозволяється публікувати статті лише українською мовою. В
    обговореннях можна використовувати будь-яку мову, якщо виникає така
    потреба, але не зловживати цим. Дозволяється користуватись як чиним
    правописом, так і офіційно не затвердженими, зокрема т.зв. [Проєктом
    1999
    року](http://uk.wikipedia.org/wiki/%D0%9F%D1%80%D0%BE%D0%B5%D0%BA%D1%82_%D0%BF%D1%80%D0%B0%D0%B2%D0%BE%D0%BF%D0%B8%D1%81%D1%83_1999) й іншими за переконанням автора, аби тільки вони
    були зрозумілими пересічному читачу.
4.  Забороняється публікування комерційної реклами за винятком випадку,
    коли автор статті надає послуги, пов'язані з темою статті: тоді
    дозволяється *ненав'язливо* повідомити про це читача виключно
    текстовими засобами обсягом не більш ніж з одне-два речення один раз
    у кінці або на початку статті.
5.  Забороняється використовувати нецензурну й вульгарну лексику та
    проводити беззмістовні провокативні дискусії.
 
